<?php
class Coffee
{
	public static function debug($label, $value)
	{
		ob_start();
		var_dump($value);
		$output = ob_get_clean();
		$label = ($label===null) ? '' : rtrim($label) . ' ';
		echo '<pre>'.htmlentities($label.$output).'</pre>';
	}

	public static function profile($label, $value)
	{
		ob_start();
		var_dump($value);
		$output = ob_get_clean();
		Profiler::log($label, "<pre>$output</pre>");
	}
}